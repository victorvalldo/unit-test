// src/main.js

const express = require('express');
const app = express();
const port = 3000;
const mylib = require('./mylib');


console.log({
    sum: mylib.sum(1, 1),
    mult: mylib.mult(2,4),
    random: mylib.random,
    array: mylib.arrayGen()
});

/*app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b); 
});
*/

app.get('/', (req, res) => {
    res.send('Hello World');
});

app.listen(port, () => {
    console.log('server: http://localhost:$(port)');

});


