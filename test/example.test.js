// test/example.test.js

const expext = require('chai').expect();
const should = require('chai').should();
const { assert } = require('chai');
const mylib = require('../src/mylib');
const { expect } = require('chai');

describe('Unit testing mylib.js', () => {

    let myvar = undefined

    it('Myvar should exist', () => {
        should.exist(myvar)
    });
    
    before(() => {
          myvar = 1; // setup before testing
         console.log('Before testing... set values to variables');
    });



    it('Should return 2 when using sum function with a=1, b=1',() => {
        const result = mylib.sum(1,1); //1+1
        expect(result).to.equal(2);  // result expected to equal 2
    });

    it('Should return 8 when using mult function with a=2, b=4',() => {
        const result = mylib.mult(2,4); // 2 * 4
        expect(result).to.equal(8);  // result expected to equal 8
    });

    it('Parametarized way of unit testing', () => {
        const result = mylib.sum(myvar, myvar); // 1+1
        expect(result).to.equal(myvar+myvar);
    })

    it('Assert lenght of foo equals 3', () => {
        assert.lengthOf('foo', 3) // true

    });


    it('Assert foo is a string', () => {
        assert.typeOf('foo', 'string') // true

    })

    after(() => {
        myvar = 0;
        console.log('After testing... clean all variables');
    })

})
